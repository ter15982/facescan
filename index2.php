<?php session_start(); 
include('condb.php');

  $ID = $_SESSION['ID'];
  $name = $_SESSION['name'];
  $level = $_SESSION['level'];
 	if($level!='admin'){
    Header("Location: logout.php");  
  }  
?>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>AdminLTE 3 | Starter</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Font Awesome Icons -->

  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
         
		
    </ul>
	  
	  <ul class="navbar-nav ml-auto"> 
	  
	  <li class="nnav-item d-none d-sm-inline-block">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fas fa-cog"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
       
        
           <div class="dropdown-divider" ></div>
         <a href="php/admin/ad_list.php" class="dropdown-item dropdown-footer">จัดการผู้ดเเล</a>
          
          <div class="dropdown-divider"></div>
          <a href="logout.php" class="dropdown-item dropdown-footer">ออกจากระบบ</a>
        </div>
      </li>
</ul>
  </nav>
  <!-- /.navbar -->
	 
	
	
	
	

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index.php" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">ระบบเช็คชื่อนักศึกษา</span>
    </a>

    <!-- Sidebar -->
    	<div class="sidebar">
  

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview ">
            <a href="#" class="nav-link">
              <i class="fas fa-paste"></i>
              <p>
                ข้อมูลการเช็คชื่อนักศึกษา
                <i class="right fas fa-angle-left"></i>	
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="php/check_student/ch_index.php" class="nav-link ">
                  <i class="fas fa-tasks "></i>
                  <p>ข้อมูลการเช็คนักศึกษา</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="php/check_student/ch_indexprint.php" class="nav-link">
                  <i class="fas fa-cloud-download-alt"></i>
                  <p>ดาวโหลดข้อมูลการเช็คชื่อ</p>
                </a>
              </li>
            </ul>
          </li>
			
			
			
			<li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fas fa-user-graduate"></i>
              <p>
                ข้อมูลนักศึกษา
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="php/student/st_list.php" class="nav-link">
                  <i class="far fa-file-alt"></i>
                  <p>รายชื่อนักศึกษา</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="php/student/Record_student.php" class="nav-link">
                  <i class="fas fa-user-plus"></i>
                  <p>เพิ่มข้อมูลนักศึกษา</p>
                </a>
              </li>
            </ul>
          </li>
			
			
			
			<li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fas fa-chalkboard-teacher"></i>
              <p>
                ข้อมูลอาจารย์
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="php/teacher/te_list.php" class="nav-link">
                  <i class="far fa-file-alt"></i>
                  <p>รายชื่ออาจารย์</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="php/teacher/Record_teacher.php" class="nav-link">
                  <i class="fas fa-user-plus"></i>
                  <p>เพิ่มข้อมูลอาจารย์</p>
                </a>
              </li>
            </ul>
          </li>
			
			
				
			<li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="far fa-address-book"></i>
              <p>
                ข้อมูลรายวิชา
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="php/subjects/su_index.php" class="nav-link">
                  <i class="far fa-file-alt"></i>
                  <p>รายวิชา</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="php/subjects/Record_subjects.php" class="nav-link">
                  <i class="fas fa-plus"></i>
                  <p>เพิ่มข้อมูลรายวิชา</p>
                </a>
              </li>
            </ul>
          </li>
			
			
			
				
			<li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fas fa-id-badge"></i>
              <p>
                ข้อมูลวิชาที่นักศึกษาเรียน
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="php/student_subjects/ss_index.php" class="nav-link">
                  <i class="far fa-file-alt"></i>
                  <p>รายชื่อการเรียนรายวิชา</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="php/student_subjects/Record_student_subjects.php" class="nav-link">
                  <i class="fas fa-plus"></i>
                  <p>เพิ่มข้อมูลการเรียนรายวิชา</p>
                </a>
              </li>
            </ul>
          </li>
			
			
			
			
			
			
			
			
			
			
			
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
   <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          
			  
			 
				  
	</form>
          </div><!-- /.col -->
     
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <div class="content">
     <div class="container-fluid">
		

<?php
echo phpinfo();
?>

	  
		<table width="100%" >
 
	<tr width="75%">
  
  <td width="15%" id="noti_number" aria-hidden="true" > </td>
	  
  
	  
  
 </tr>
</table>  
			  
			  
			  
    
	<!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

 
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script type="text/javascript">
 function loadDoc() {
  

  setInterval(function(){

   var xhttp = new XMLHttpRequest();
   xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("noti_number").innerHTML = this.responseText;
    }
   };
   xhttp.open("GET", "check/php/show_rawtimeTest/data.php", true);
   xhttp.send();

  },1000);


 }
 loadDoc();
</script>
</body>
</html>
